'use strict';

var sgeo = require('sgeo');
var config = require('./config.json');

var home = config.HOME_LOCATION;
home = home && home.length === 2 && home[0] && home[1]
    ? new sgeo.latlon(home[0], home[1])
    : null;

const KM_PER_MILE = 1.60934;
const YARD_PER_MILE = 1760;

const HEADINGS = {
  0: 'N',
  22.5: 'NNE',
  45: 'NE',
  67.5: 'ENE',
  90: 'E',
  112.5: 'ESE',
  135: 'SE',
  157.5: 'SSE',
  180: 'S',
  202.5: 'SSW',
  225: 'SW',
  247.5: 'WSW',
  270: 'W',
  292.5: 'WNW',
  315: 'NW',
  337.5: 'NNW',
  360: 'N' //List north twice so we don't have to do weird modulo distances
};

function getDirection(bearing) {
  var low = 0;
  var lowName = 'N';
  var high = 360;
  var highName = 'N';

  for (var heading in HEADINGS) {
    if (HEADINGS.hasOwnProperty(heading)) {
      heading = parseFloat(heading);
      if (heading > low && heading <= bearing) {
        low = heading;
        lowName = HEADINGS[heading];
      }
      if (heading < high && heading >= bearing) {
        high = heading;
        highName = HEADINGS[heading];
      }
    }
  }
  var highDistance = (high - bearing) * highName.length; //Use length of name to bias results towards shorter labels
  var lowDistance = (bearing - low) * lowName.length;
  return lowDistance < highDistance ? lowName : highName;
}

function distanceTo(lat, lon) {
  var point = new sgeo.latlon(lat, lon);
  return home.distanceTo(point) / KM_PER_MILE;
}

function offset(bearing, distance) {
  return home.destinationPoint(bearing, distance);
}

function bearingTo(lat, lon) {
  var point = new sgeo.latlon(lat, lon);
  return home.bearingTo(point);
}

function displayString(distance, bearing) {
  var distanceStr;
  if (distance < 0.5) {
    distanceStr = Math.floor(distance * YARD_PER_MILE) + 'yd';
  } else if (distance < 10) {
    distanceStr = Number(distance).toFixed(1) + 'mi';
  } else {
    distanceStr = Math.round(distance) + 'mi';
  }
  return distanceStr + ' ' +  getDirection(bearing);
}

module.exports = {
  distanceTo,
  bearingTo,
  displayString,
  offset
};