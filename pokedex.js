'use strict';

var sqlite3 = require('sqlite3');
var fs = require('fs');

var pokedex = require('./pokedex.json');
var config = require('./config.json');

const DB_FILE = 'frequencies.db';

//Connect to database
var dbExists = fs.existsSync(DB_FILE);
var db = new sqlite3.Database(DB_FILE);

//Frequencies for all pokemon types
var frequencies = {};
var totalSpotted = 0;

function getAncestors(id) {
  var pokemon = pokedex[id];
  var ancestors = [];
  for (var i = 0; i < pokemon.from.length; i++) {
    var ancestorId = pokemon.from[i];
    ancestors.push(ancestorId);
    var recurse = getAncestors(ancestorId);
    ancestors = ancestors.concat(recurse);
  }
  return ancestors;
}

pokedex.saveCounts = function(ids) {
  var stmt = db.prepare('UPDATE seen SET freq = freq + ? WHERE id = ?');
  var grouped = {};
  for (var i = 0; i < ids.length; i++) {
    var id = ids[i];
    if (grouped[id]) {
      grouped[id]++;
    } else {
      grouped[id] = 1;
    }
  }

  for (var saveId in grouped) {
    if (grouped.hasOwnProperty(saveId)) {
      var num = grouped[saveId];
      totalSpotted += num;
      frequencies[saveId] += num;
      stmt.run(num, saveId);
    }
  }
  stmt.finalize();
};

function getFrequency(id) {
  if (totalSpotted < config.CALIBRATION_THRESHOLD) {
    //Until I've hit my calibration threshold, every pokemon is common
    return 100;
  }
  return frequencies[id] / totalSpotted * 100;
}

pokedex.getFrequency = getFrequency;

pokedex.getAncestorFrequency = function(id) {
  return Math.max.apply(null, getAncestors(id).map(getFrequency));
};

module.exports = function(callback) {
  db.serialize(function() {
    if (!dbExists) {
      //Initialize database, save all frequencies of zero
      console.log('Initializing database...');
      db.run('CREATE TABLE seen (id INTEGER, freq INTEGER)');
      var stmt = db.prepare('INSERT INTO seen (id, freq) VALUES (?, 0)');
      var maxId = Object.keys(pokedex).length;
      for (var insertId = 1; insertId <= maxId; insertId++) {
        stmt.run(insertId);
        frequencies[insertId] = 0;
      }
      stmt.finalize();
      console.log('done.');
    }

    //Grab existing frequency counts
    console.log('Loading frequency counts from database...');

    db.all('SELECT id, freq FROM seen', function(err, rows) {
      for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        frequencies[row.id] = row.freq;
        totalSpotted += row.freq;
      }
      console.log('done with ' + totalSpotted + ' sightings loaded');

      callback(pokedex);
    });
  });
};
