'use strict';

var request = require('request');
var md5 = require('js-md5');

var config = require('./config.json');
var print = require('./slack.js');

//To keep track of specific pokemon that we've seen in previous requests
var alreadySeen = {};

var sleeping = null;
function checkTime() {
  var hour = (new Date()).getHours();
  var newValue = hour < config.WAKE_UP || hour >= config.SLEEP;
  if (sleeping === null) {
    //Check for initial value of null so that it doesn't print these messages when it first comes up
    sleeping = newValue;
  } else if (sleeping !== newValue) {
    sleeping = newValue;
    print(sleeping
        ? ('pokewatcher sleeping until ' + config.WAKE_UP + ':00 :clock' + config.WAKE_UP + ': :sleeping:')
        : 'pokewatcher starting up :sunny: :coffee:'
    );
  }
}

//Keep track of whether or not the API is working
var apiUp = true;
var apiChangeCount = 0;
function trackAPIStatus(success) {
  if (apiUp == success) {
    apiChangeCount = Math.max(0, apiChangeCount - 2);
    return;
  }

  apiChangeCount++;
  if (apiChangeCount >= 5) {
    apiChangeCount = 0;
    apiUp = !apiUp;
    print(apiUp ? 'API up :smiley:' : 'API down :disappointed:');
  }
}

function fetchData(callback) {
  request('http://172.16.50.136:5001/map-data', function(error, response, body) {
    if (!error && response.statusCode == 200) {
      try {
        var parsed = JSON.parse(body);
        trackAPIStatus(true);
        callback(parsed);
        return;
      } catch (ex) {
      }
    }
    trackAPIStatus(false);
  });
}

function expireAlreadySeen() {
  var now = Date.now() / 1000;
  for (var uid in alreadySeen) {
    if (alreadySeen.hasOwnProperty(uid) && alreadySeen[uid] <= now) {
      delete alreadySeen[uid];
    }
  }
}

function processResult(response, pokedex) {
  var now = Math.ceil(Date.now() / 1000);
  var newIds = [];

  for (var i = 0; i < response.pokemons.length; i++) {
    var mon = response.pokemons[i];

    var uniqueId = md5([
      mon.pokemon_id,
      mon.latitude,
      mon.longitude
    ]);

    if (alreadySeen[uniqueId]) {
      //We've already seen this one
      continue;
    }
    alreadySeen[uniqueId] = mon.disappear_time;
    newIds.push(mon.pokemon_id);

    //TODO: push these frequency lookups into print?
    var freq = pokedex.getFrequency(mon.pokemon_id);
    //FIXME: ancestor frequencies appear to be broken currently
    var afreq = pokedex.getAncestorFrequency(mon.pokemon_id);

    var sighting = {
      id: mon.pokemon_id,
      lat: mon.latitude,
      lon: mon.longitude,
      duration: Math.ceil(mon.disappear_time / 1000) - now,
      frequency: freq,
      ancestorFrequency: afreq
    };

    print(sighting, pokedex);
  }

  pokedex.saveCounts(newIds);
}

module.exports = function(pokedex) {
  checkTime();
  if (sleeping) {
    return;
  }
  expireAlreadySeen();
  fetchData(function(response) {
    processResult(response, pokedex);
  });
};
