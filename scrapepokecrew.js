var request = require('request');
var btoa = require('btoa');

var pokemonID = 1;
var images = {};

function finish() {
  //TODO: fs.write this
  console.log(JSON.stringify(images));
}

function checkImage() {
  var pokeString;
  if (pokemonID < 10) {
    pokeString = '00' + pokemonID;
  } else if (pokemonID < 100) {
    pokeString = '0' + pokemonID;
  } else {
    pokeString = pokemonID;
  }
  request({
    method: 'GET',
    uri: 'https://s3.amazonaws.com/pokecrew-prod/pokemons/previews/000/000/' + pokeString + '/original/data'
  }, function(err, resp, body) {
    if (err || resp.statusCode !== 200) {
      finish();
    } else {
      images[pokemonID] = 'data:image/*;base64,' + btoa(body);
      pokemonID++;
      checkImage(); //recurse
    }
  });
}

//Main
console.log('Fetching images from pokecrew...');
checkImage();
