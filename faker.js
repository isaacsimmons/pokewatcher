'use strict';

var frequencies = require('./pokedex');
var slack = require('./slack');

var config = require('./config.json');

var geo = require('./geo');

var RARES = [1, 2, 3];

var bearing = Math.random() * 360;
var distance = Math.random() * 0.5 + 0.2; //between 0.2 and 0.7 km
var duration = Math.random() * 9 * 60 + 5 * 60; //between 5 and 14 mins

var pokeID = RARES[Math.floor(Math.random() * RARES.length)];

var fakePoint = geo.offset(bearing, distance);

console.log(fakePoint.lat + ', ' + fakePoint.lng);


//Main
frequencies(function(pokedex) {
  slack({
    id: pokeID,
    lat: fakePoint.lat,
    lon: fakePoint.lng,
    duration: duration,
    frequency: 0.01, //TODO: slack API should deal with loading this from pokedex
  }, pokedex);
});
