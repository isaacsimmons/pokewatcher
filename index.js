'use strict';

var frequencies = require('./pokedex');
var checkAPI = require('./pokevision');

var config = require('./config.json');

//Main
frequencies(function(pokedex) {
  checkAPI(pokedex);
  setInterval(function() { checkAPI(pokedex); }, config.UPDATE_FREQUENCY * 1000);
});