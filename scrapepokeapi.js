var request = require('request');

var pokemonID = 1;
var pdex = {};

function reverseEvolutionLinks() {
  for (var id in pdex) {
    if (pdex.hasOwnProperty(id)) {
      var entry = pdex[id];
      for (var i = 0; i < entry.to.length; i++) {
        var to = entry.to[i];
        if (!pdex[to]) {
          console.log('Warning: Got evolve data from ' + id + ' to ' + to + ', but have no entry for destination');
          continue;
        }
        pdex[to].from.push(parseInt(id));
      }
    }
  }
}

function finish() {
  reverseEvolutionLinks();
  //TODO: fs.write this
  console.log(JSON.stringify(pdex));
}

function checkPokedex() {
  request({
    method: 'GET',
    uri: 'http://pokeapi.co/api/v1/pokemon/' + pokemonID + '/'
  }, function(err, resp, body) {
    if (err || resp.statusCode !== 200) {
      finish();
    } else {
      var parsed = JSON.parse(body);
      var i;
      var types = [];
      for (i = 0; i < parsed.types.length; i++) {
        types.push(parsed.types[i].name);
      }
      var to = [];
      var toUnique = {};
      for (i = 0; i < parsed.evolutions.length; i++) {
        var evolution = parsed.evolutions[i];
        if (evolution.resource_uri && evolution.resource_uri.indexOf('/api/v1/pokemon/') === 0) {
          var parts = evolution.resource_uri.split('/');
          if (parts.length === 6) {
            var evolvesTo = parseInt(parts[4]);
            if (pokemonID == evolvesTo) {
              //Not sure if any have this, but I don't care about any "self-evolves"
              continue;
            }
            if (!toUnique[evolvesTo]) {
              to.push(evolvesTo);
              toUnique[evolvesTo] = true;
            }
          }
        }
      }
      pdex[pokemonID] = {name: parsed.name, types: types, to: to, from: []};
      pokemonID++;
      checkPokedex(); //recurse
    }
  });
}

//Main
console.log('Fetching data from pokeapi...');
checkPokedex();
