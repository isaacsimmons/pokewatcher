'use strict';

var request = require('request');
var config = require('./config.json');
var geo = require('./geo');

var WALKING_KM_PER_S = 3.1 * 0.00044704; //3.1mph

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  return hours + ':' + minutes + ampm;
}

function formatMinsSecs(seconds) {
  var durationMin = Math.floor(seconds / 60);
  var durationSec = Math.floor(seconds) % 60;
  //Padding
  if (durationSec < 10) {
    durationSec = '0' + durationSec;
  }
  //TODO: some rounding?
  return durationMin + 'm' + durationSec + 's';
}

function buildNotification(sighting, pokedex) {
  //Unpack the params object
  var {id, lat, lon, duration, frequency} = sighting;


  if (frequency > config.COMMON_THRESHOLD) {
    //No alerts for common pokemon
    return false;
  }
  // if (sighting.ancestorFrequency > config.COMMON_ANCESTOR_THRESHOLD) {
  //   //No alerts for evolved forms of very common pokemon
  //   return false;
  // }

  var distance = geo.distanceTo(lat, lon);
  var bearing = geo.bearingTo(lat, lon);
  var walkTime = distance / WALKING_KM_PER_S;

  if (duration < walkTime + 15) {
    //Don't bother mentioning anything with less than 60 seconds remaining until despawn
    //TODO: also incorporate the distance to "home" in this calculation if specified
    return false;
  }

  var link = 'https://maps.googleapis.com/maps/api/staticmap?'
      + 'center=' + lat + ',' + lon
      + '&zoom=15&size=600x300&maptype=roadmap' //TODO: zoom dynamically base on the distance if a "home" is defined?
      + '&key=' + config.GMAPS_STATIC_API_KEY
      + '&markers=color:red%7Clabel:' + pokedex[id].name[0] + '%7C' + lat + ',' + lon
      + '&markers=color:green%7C' + config.HOME_LOCATION[0] + ',' + config.HOME_LOCATION[1];
  var distanceStr = geo.displayString(distance, bearing);
  //TODO: maybe displayString belongs in this class instead of geo?
  var linkText = ' <' + link + '|' + distanceStr + '>';

  var alert = '';
  if (frequency < config.RARE_THRESHOLD) {
    alert = '<!here|here> ';
  } else if (distance > 1) {
    //1km absolute distance threshold for medium-rare pokeman
    return false;
  }

  var durationString = ':stopwatch:' + formatMinsSecs(duration);
  var expirationString = ':clock430:' + formatAMPM(new Date((new Date()).getTime() + duration * 1000));
  var walkTimeString = ':walking:' + formatMinsSecs(walkTime);

  var message = alert + '`#' + id + '` ' + pokedex[id].name
      + ' (' + (frequency > 0.1 ? Number(frequency).toFixed(1) : '<0.1') + '%) '
      + linkText + ' ' + walkTimeString + ' '
      + expirationString + ' ' + durationString;

  var pokeString;
  if (id < 10) {
    pokeString = '00' + id;
  } else if (id < 100) {
    pokeString = '0' + id;
  } else {
    pokeString = id;
  }

  return {
    attachments: [{
      fallback: message,
      thumb_url: 'https://s3.amazonaws.com/pokecrew-prod/pokemons/previews/000/000/' + pokeString + '/original/data',
      title: alert + '#' + id + ' ' + pokedex[id].name + ' (' + (frequency > 0.1 ? Number(frequency).toFixed(1) : '<0.1') + '%) ' + distanceStr,
      title_link: link,
      color: '#7CD197', //TODO: color by severity?
      text: walkTimeString + ' ' + durationString + ' ' + expirationString,
    }]
  };
}

module.exports = function(message, pokedex) {
  var payload;
  if (typeof message === 'object') {
    //If we got a pokemon sighting, translate it into a slack message
    payload = buildNotification(message, pokedex);
  } else {
    payload = {text: message};
  }

  if (payload === false) {
    //If the notification wasn't deemed worth printing, exit
    return;
  }

  console.log(payload);

  //Only actually print to slack if a webhook URL is configured
  if (!config.SLACK_WEBHOOK) {
    return;
  }

  request({
    method: 'POST',
    uri: config.SLACK_WEBHOOK,
    "content-type": 'application/json',
    body: JSON.stringify(payload)
  });
};
